#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void numberInput(int& firstInput, int& secondInput);
void firstInputRangeCheck(int& firstInput);
void secondInputRangeCheck(int& secondInput);
int calculateSolution(int& firstInput, int& secondInput);
int calculateSolutionRec(const int& firstInput,const int& secondInput,
                         const std::vector<int>& vector_t, std::vector<int>& current,
                         int position, int& count);

/* This part of the program initializes the rest of the program 
and calls all the functions used in the program */

int main() {
  while (true) {
    int firstInput;
    int secondInput;
    numberInput(firstInput, secondInput);
    int solution = calculateSolution(firstInput, secondInput);
    std::cout << "This is the answer" << '\n';
    std::cout << solution << '\n';
    std::cout << "Do you want to continue? Type Y or N" << '\n';
    std::string answer;
    std::cin >> answer;
    if (answer == "N" || answer == "n" ) break;
  }
    std::cout << "Hope you liked the program" << '\n';
    return 0;
}

/* This method allows you to input both the values of X and N and 
then calls other methods to check if X and N are within the required range.
*/
void numberInput(int& firstInput, int& secondInput) {
    std::cout << "This program finds the number of ways a given integer X" << '\n';
    std::cout << "can be expressed as the sum of the Nth power of " << '\n';
    std::cout << "unique natural numbers " << '\n';
    std::cout << "Please enter a integer X between 1 and 1000 inclusive" << '\n';
    std::cin >> firstInput;
    firstInputRangeCheck(firstInput);
    std::cout << "Please enter a integer N between 2 and 10 inclusive" << '\n';
    std::cin >> secondInput;
    secondInputRangeCheck(secondInput);
}

/* This method checks if the value of X input is within the required range. 
It then prompts the user to re-enter the value of X if it falls out of range
*/

void firstInputRangeCheck(int& firstInput) {
    while (std::cin.fail()) {
        std::cout << "Please enter an Integer" << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> firstInput;
    }
    if (firstInput < 1 || firstInput > 1000) {
        std::cout << "The number should be between 1 and 1000 inclusive" << '\n';
        std::cout << "Please enter a valid integer" << '\n';
        std::cin >> firstInput;
        firstInputRangeCheck(firstInput);
    }
}

/* This method checks if the value of N input is within the required range. 
It then prompts the user to re-enter the value of N if it falls out of range
*/

void secondInputRangeCheck(int& secondInput) {
    while (std::cin.fail()) {
        std::cout << "Please enter an Integer" << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> secondInput;
    }
    if (secondInput < 2 || secondInput > 10) {
        std::cout << "The number should be between 2 and 10 inclusive" << '\n';
        std::cout << "Please enter a valid integer" << '\n';
        std::cin >> secondInput;
        secondInputRangeCheck(secondInput);
    }
}

/* This method acts sets up the vectors to be used in the recursive call 
and it also sets up the counter(count) that keeps track of the number of
viable solutions found.It also determines the space to be allocated to the
vector which is determined but the values of X and N.  
*/
int calculateSolution(int& firstInput, int& secondInput) {
    std::vector<int> vector_t;
    std::vector<int> current;
    int numbers = 0;
    for (int i = 1; i <= firstInput; i++) {
        vector_t.push_back(i);
        if (pow(i, secondInput) > firstInput) {
            numbers = i;
            break;
        }
    }
    int count = 0;
    return calculateSolutionRec(firstInput, secondInput, vector_t, current,
                                numbers,count);
}

/* This method recursively calls itself to generate all possible combinations 
of elements contained in the vector_t. The sum of each combination is then tested
with the value of N and if found to be work, the counter is increased.
*/
int calculateSolutionRec(const int& firstInput,const int& secondInput,
                         const std::vector<int>& vector_t, std::vector<int>& current,
                         int position, int& count) {
    if (vector_t.empty()) {
        int total = 0;
        for(int i = 0; i < current.size(); i++) {
            total += pow(current[i], secondInput);
            if (total > firstInput) break;
        }
        if (total == firstInput) {
            std::cout << "This is one possible solution" << '\n';
            for (int i = 0; i < current.size(); i++) {
                std::cout << current[i] << '\n';
            }
            count++;
        }
    } else {
        std::vector<int> vector_tCopy;
        std::vector<int> currentCopy = current;
        int number = vector_t[0];
        for (int i = 1; i < vector_t.size(); i++) {
            vector_tCopy.push_back(vector_t[i]);
        }
        calculateSolutionRec(firstInput,secondInput,vector_tCopy,currentCopy,
                             position, count);
        currentCopy.push_back(number);
        calculateSolutionRec(firstInput,secondInput,vector_tCopy,currentCopy,
                             position - 1, count);
    }
    return count;
}
